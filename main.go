package main

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gitlab.com/omame/dowser/internal/api"
	"gitlab.com/omame/dowser/internal/config"
)

func main() {
	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp: false,
	})

	args := config.ParseArgs()
	log.SetLevel(log.InfoLevel)
	if args.Debug {
		log.SetLevel(log.DebugLevel)
	}

	client, err := api.NewClient(args)
	if err != nil {
		log.Fatalf("Failed to creat a backend client: %s", err)
	}

	prometheus.MustRegister(client)

	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`OK`))
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
	         <head><title>Dowser</title></head>
	         <body>
	         <h1>Dowser</h1>
	         <p><a href='/metrics'>Metrics</a></p>
	         </body>
	         </html>`))
	})

	log.Println("dowser listening on", args.Address)
	log.Fatal(http.ListenAndServe(args.Address, nil))
}
