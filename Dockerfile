FROM alpine:3.13

EXPOSE 9095/tcp

RUN apk --no-cache add ca-certificates libc6-compat

COPY dowser /

ENTRYPOINT [ "/dowser" ]
