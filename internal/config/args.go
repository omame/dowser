package config

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

// Args contains the command line arguments
type Args struct {
	Address   string
	Backend   string
	Debug     bool
	Locations []string
}

// ParseArgs parses the command line arguments
func ParseArgs() Args {
	var args Args
	var locations string

	flag.BoolVar(&args.Debug, "debug", false, "enable debug level logging")
	flag.StringVar(&args.Address, "address", ":9095", "listening address")
	flag.StringVar(&args.Backend, "backend", "", "the weather service to use in order to fetch the rain status")
	flag.StringVar(&locations, "locations", "", "comma-separated list of locations to check")

	flag.Usage = func() {
		fmt.Printf("Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.Parse()

	if args.Backend == "" {
		fmt.Println("please specify the desired weather backend")
		os.Exit(1)
	}

	if locations == "" {
		fmt.Println("please specify at least one location")
		os.Exit(1)
	}
	args.Locations = strings.Split(locations, ",")

	return args
}
