package metrics

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	namespace = "dowser"
)

// Prometheus metrics
var (
	BootTime = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "boot_time_seconds",
		Help:      "unix timestamp of when the service was started",
	})

	ClientErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "client_errors_total",
			Help:      "Number of errors received from the backend API.",
		}, []string{"reason"})

	ClientRequests = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "client_requests_total",
			Help:      "Number of requests to the backend API.",
		})

	ExporterUp = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "up",
		Help:      "Whether the service is ready to receive requests or not",
	})

	Rain = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "rain",
		Help:      "Rain status for a location",
	}, []string{"location"})
)

func init() {
	BootTime.Set(float64(time.Now().Unix()))
	ExporterUp.Set(0)
}
