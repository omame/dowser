package api

import (
	"fmt"

	"gitlab.com/omame/dowser/internal/config"
	"gitlab.com/omame/dowser/internal/weatherservice"
)

type APIClient struct {
	Locations      []string
	WeatherService weatherservice.WeatherService
}

// NewClient creates a new API client to a weather service
func NewClient(args config.Args) (APIClient, error) {
	switch args.Backend {
	case "accuweather":
		ws, err := weatherservice.GetAccuweatherClient()
		if err != nil {
			return APIClient{}, err
		}
		return APIClient{
			Locations:      args.Locations,
			WeatherService: ws,
		}, nil
	default:
		return APIClient{}, fmt.Errorf("unknown weather service: %s", args.Backend)
	}
}
