package api

import (
	"fmt"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/omame/dowser/internal/metrics"
)

const (
	namespace = "dowser"
)

// Describe implements Collector interface.
func (a APIClient) Describe(ch chan<- *prometheus.Desc) {
	metrics.BootTime.Describe(ch)
	metrics.ClientErrors.Describe(ch)
	metrics.ClientRequests.Describe(ch)
	metrics.ExporterUp.Describe(ch)
}

// Collect collects rain metrics
func (a APIClient) Collect(ch chan<- prometheus.Metric) {
	metrics.ExporterUp.Set(1)
	var wg sync.WaitGroup
	wg.Add(len(a.Locations))
	for _, location := range a.Locations {
		go func(location string) {
			defer wg.Done()
			metrics.ClientRequests.Inc()
			isItRaining, err := a.WeatherService.GetRainStatus(location)
			if err != nil {
				metrics.ClientErrors.WithLabelValues(fmt.Sprintf("%s", err)).Inc()
			}
			if isItRaining {
				metrics.Rain.WithLabelValues(location).Set(1)
				return
			}
			metrics.Rain.WithLabelValues(location).Set(0)
		}(location)
	}
	wg.Wait()

	metrics.BootTime.Collect(ch)
	metrics.ClientErrors.Collect(ch)
	metrics.ClientRequests.Collect(ch)
	metrics.ExporterUp.Collect(ch)
	metrics.Rain.Collect(ch)
}
