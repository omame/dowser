package weatherservice

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const (
	clientTimeoutSeconds = 2
	baseUrl              = "http://dataservice.accuweather.com/"
	currentConditionsUrl = baseUrl + "currentconditions/v1/%s?apikey=%s&details=true"
)

type accuweatherClient struct {
	apiKey     string
	httpClient http.Client
}

type CurrentConditions []CurrentCondition

type CurrentCondition struct {
	HasPrecipitation bool `json:"HasPrecipitation"`
}

func GetAccuweatherClient() (accuweatherClient, error) {
	apiKey := os.Getenv("ACCUWEATHER_API_KEY")
	if apiKey == "" {
		return accuweatherClient{}, fmt.Errorf("please set the ACCUWEATHER_API_KEY environment variable")
	}

	return accuweatherClient{
		apiKey: apiKey,
		httpClient: http.Client{
			Timeout: time.Second * clientTimeoutSeconds,
		},
	}, nil

}

func (c accuweatherClient) GetRainStatus(location string) (bool, error) {
	url := fmt.Sprintf(currentConditionsUrl, location, c.apiKey)
	res, err := c.httpClient.Get(url)
	if err != nil {
		return false, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return false, err
	}

	response := &CurrentConditions{}
	err = json.Unmarshal(body, response)
	if err != nil {
		return false, err
	}

	for _, location := range *response {
		return location.HasPrecipitation, nil
	}
	return false, nil // This is dead code
}
