package weatherservice

// WeatherService is an interface for a generic API client
type WeatherService interface {
	GetRainStatus(location string) (bool, error)
}
